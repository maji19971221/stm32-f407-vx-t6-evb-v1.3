# STM32F407VxT6-EVB-V1.3

### 简介
以下代码为个人学习的STM32F407的代码。

# 更新日志
## 2021/09/16
#### 驱动.zip
F407HAL的一些驱动代码
#### FreeRTOS.zip
FreeRTOS源码
#### lwip-2.1.2.zip
lwip以太网协议栈源码
#### STM32F407_Demo
STM32F407裸机程序，上电串口1打印。
#### STM32F407_ETH)LEIP_PING
STM32F407lwip无操作系统移植代码。可以ping通。
#### STM32F407_FreeRTOS_Demo
STM32F407RTOS系统移植。闪灯。
## 2021/11/27
#### STM32F407_FreeRTOS_Demo -2
FreeRTOS任务挂起与恢复
#### STM32F407_FreeRTOS_Demo -3
消息队列的使用，定义了两个消息队列，按键触发。一个线程闪灯表示程序运行，一个现成用于根据按键键值往不同的消息队列里发送数据，两个线程用于接收两个消息队列的数据。